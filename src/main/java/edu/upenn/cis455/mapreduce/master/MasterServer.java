package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.print.event.PrintEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.PrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.FileSpoutImpl;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

public class MasterServer {
  static Logger log = LogManager.getLogger(MasterServer.class);
  static final long serialVersionUID = 455555001;
  static final int myPort = 8000;
  
  private static final String SPOUT = "SPOUT";
  private static final String MAP_BOLT = "MAP_BOLT";
  private static final String REDUCE_BOLT = "REDUCE_BOLT";
  private static final String PRINT_BOLT = "PRINT_BOLT";
  
  static Map<String, WorkerStatus> statuses = new HashMap<String, WorkerStatus>();
  static Config config;
  
  public static void registerStatusPage() {
		get("/status", (request, response) -> {
            response.type("text/html");
            StringBuilder sb = new StringBuilder();
            sb.append("<html><head><title>Master</title></head>\n" +
            		"<body> KUNAL NAIK <br>");
            		
            sb.append("<form action=\"/submit\" method=\"get\" id=\"form1\">");
            sb.append("Job Class Name: <input type=\"text\" name=\"job\"><br>");
            sb.append("Input Directory: <input type=\"text\" name=\"inputDirectory\"><br>");
            sb.append("Output Directory: <input type=\"text\" name=\"outputDirectory\"><br>");
            sb.append("Number of Map Threads: <input type=\"text\" name=\"numMapThreads\"><br>");
            sb.append("Number of Reduce Threads: <input type=\"text\" name=\"numReduceThreads\"><br>");
            sb.append("<button type=\"submit\" form=\"form1\" value=\"Submit\">Submit</button>");
            sb.append("</form>");
            
            sb.append("<table><tr>");
            sb.append("<th> IP:Port </th>");
            sb.append("<th> Status </th>");
            sb.append("<th> Job </th>");
            sb.append("<th> Keys Read </th>");
            sb.append("<th> Keys Written </th>");
            sb.append("</tr>");
                
            for(String ip : statuses.keySet()){
                WorkerStatus curr = statuses.get(ip);
                sb.append("<tr>");
                sb.append("<td> " + curr.ip + ":" + curr.port + " </td>");
                sb.append("<td> " + curr.status + " </td>");
                sb.append("<td> " + curr.job + " </td>");
                sb.append("<td> " + curr.keysRead + " </td>");
                sb.append("<td> " + curr.keysWritten + " </td>");
                sb.append("</tr>");
            }
            sb.append("</table></body></html>");
        
            return sb.toString();
		});
		
		redirect.get("/", "/status");
		registerSubmit();
  }
  
  public static void registerSubmit() {
      get("/submit", (request, response) -> {
          String jobClass = request.queryParams("job");
          String inputDirectory = request.queryParams("inputDirectory");
          String outputDirectory = request.queryParams("outputDirectory");
          String numMapThreads = request.queryParams("numMapThreads");
          String numReduceThreads = request.queryParams("numReduceThreads");
          
          config.put("job", jobClass);
          config.put("mapClass", jobClass);
          config.put("reduceClass", jobClass);
          config.put("spoutExecutors", "1");
          config.put("mapExecutors", numMapThreads);
          config.put("reduceExecutors", numReduceThreads);
          config.put("inputDirectory", "store/" + inputDirectory);
          config.put("outputDirectory", "store/" + outputDirectory);
        
          // Create Topology based on this information
          FileSpoutImpl spout = new FileSpoutImpl();
          // spout.setFilename(inputDirectory);
          MapBolt bolt = new MapBolt();
          ReduceBolt bolt2 = new ReduceBolt();
          PrintBolt printer = new PrintBolt();   


          TopologyBuilder builder = new TopologyBuilder();
          // Only one source ("spout") for the words
          builder.setSpout(SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));
          // Parallel mappers, each of which gets specific words
          builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(SPOUT, new Fields("value"));
          // Parallel reducers, each of which gets specific words
          builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

          builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);
          
          Topology topo = builder.createTopology();
          
          // send in the config into the Worker Job
          WorkerJob job = new WorkerJob(topo, config);
          
          // Create the workers necessary for this job
          ObjectMapper mapper = new ObjectMapper();
	        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
			try {
				String[] workers = WorkerHelper.getWorkers(config);
	
				int i = 0;
				for (String dest: workers) {
			        config.put("workerIndex", String.valueOf(i++));
					if (sendJob(dest, "POST", config, "definejob", 
							mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job definition request failed");
					}
				}
				
				for (String dest: workers) {
					if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
							HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job execution request failed");
					}
				}
				
			} catch (JsonProcessingException e) {
				e.printStackTrace();
		        System.exit(0);
			}
			
          response.redirect("/status");
          return null;
      });

  }

  public static void registerShutdown() {
        get("/shutdown", (request, response) -> {
            String[] workers = WorkerHelper.getWorkers(config);
		    for (String dest: workers) {
                if(sendJob(dest, "GET", config, "shutdown", 
    							null).getResponseCode() != 
    							HttpURLConnection.HTTP_OK){
    			     System.out.println("Could not shut down");
    			}	
		  }
		  return "shutdown";
        });
    }
    


  
  public static void registerWorkerStatus() {
		get("/workerstatus", (request, response) -> {
            String port = request.queryParams("port");
            String status = request.queryParams("status");
            String job = request.queryParams("job");
            String keysRead = request.queryParams("keysRead");
            String keysWritten = request.queryParams("keysWritten");
            String results = request.queryParams("results");
            String ip = request.ip().substring(0, request.ip().length()-1) + port;
            
            WorkerStatus currentWorker = statuses.get(ip);
            if(currentWorker != null){
                currentWorker.update(port, status, job, keysRead, keysWritten, results, ip);
            } else {
                currentWorker = new WorkerStatus(port, status, job, keysRead, keysWritten, results, ip);
            }
            statuses.put(ip, currentWorker);
            
            // System.out.println("received request from " + port);
            return "worker registered";
		});
		
  }

  /**
   * The mainline for launching a MapReduce Master.  This should
   * handle at least the status and workerstatus routes, and optionally
   * initialize a worker as well.
   * 
   * @param args
 * @throws IOException
   */
    public static void main(String[] args) throws IOException {
        String filename = ""; 
		port(myPort);
		
		System.out.println("Master node startup on " + myPort);
		
		// TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce

		config = new Config();
        
        // Complete list of workers, comma-delimited
        config.put("workerList", "[127.0.0.1:8001,127.0.0.1:8002]");
        // Build the local worker
        config.put("workerIndex", "0");
        // WorkerServer.createWorker(config);
        
        config.put("master", "127.0.0.1:8000");
        registerStatusPage();
        registerWorkerStatus();
		     
        System.out.println("Press [Enter] to shut down this node...");
		(new BufferedReader(new InputStreamReader(System.in))).readLine();
		WorkerServer.shutdown();
		

	}
	
	static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		System.out.println("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();
		
		return conn;
    }

}
  
