package edu.upenn.cis455.mapreduce.master;

import java.time.Instant;

public class WorkerStatus {
    String port;
    String status;
    String job;
    String keysRead;
    String keysWritten;
    String results;
    String ip;
    long lastSent; 
    
    public WorkerStatus(String port, String status, String job, String keysRead, String keysWritten, String results,
        String ip){
        this.port = port;
        this.status = status; 
        this.job = job; 
        this.keysRead = keysRead; 
        this.keysWritten = keysWritten; 
        this.results = results; 
        this.ip = ip;
        this.lastSent = Instant.now().toEpochMilli();
    }
    
    public void update(String port, String status, String job, String keysRead, String keysWritten, String results,
        String ip){
        this.port = port;
        this.status = status; 
        this.job = job; 
        this.keysRead = keysRead; 
        this.keysWritten = keysWritten; 
        this.results = results; 
        this.ip = ip;
        this.lastSent = Instant.now().toEpochMilli();
    }
    
    public boolean withinThirty(){
        return this.lastSent + 30000 > Instant.now().toEpochMilli();
    }
    
}
