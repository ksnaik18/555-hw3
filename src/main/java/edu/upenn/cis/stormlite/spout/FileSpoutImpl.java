package edu.upenn.cis.stormlite.spout;

public class FileSpoutImpl extends FileSpout{
    
	@Override
	public String getFilename() {
		return "words.txt";
	}
}
