package edu.upenn.cis.stormlite.storage;

public class StorageFactory {
    public static StorageInterface getDatabaseInstance(String directory) {
        return new StorageImpl(directory);
    }
}
