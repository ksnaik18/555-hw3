package edu.upenn.cis.stormlite.storage;

import java.util.List;
import java.util.Map;


public interface StorageInterface {
    
    /**
	 * Add a new key-value
	 */
	public int add(String key, String value);
	
	public List<String> get(String key);
	
	public void close();
}
