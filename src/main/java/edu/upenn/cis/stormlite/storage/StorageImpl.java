package edu.upenn.cis.stormlite.storage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.model.DBViews;


public class StorageImpl implements StorageInterface{
    final static Logger logger = LogManager.getLogger(StorageImpl.class);
    private Environment environment = null;
    private Database mapDB = null;
    private DBViews dbViews = null;

    private static final String CLASS_CATALOG = "java_class_catalog";
    private Database catalogDatabase = null;
    
    public StorageImpl(String path){
        try {
         EnvironmentConfig envConfig = new EnvironmentConfig();
         envConfig.setAllowCreate(true);
         envConfig.setTransactional(true);
         this.environment = new Environment(new File(path), envConfig);
         
         // Open the user database. Create it if it does not already exist.
         DatabaseConfig dbConfig = new DatabaseConfig();
         dbConfig.setAllowCreate(true);
         this.mapDB = this.environment.openDatabase(null, "mapDB", dbConfig);
         this.catalogDatabase = this.environment.openDatabase(null, CLASS_CATALOG, dbConfig);
         
         //Create the Db View that holds database maps
         this.dbViews = new DBViews(catalogDatabase, mapDB);

        } catch (DatabaseException dbe) {
            logger.info("DatabaseException in StorageImpl: " + dbe.getMessage());
            System.out.println("DatabaseException in StorageImpl: ");
        } 
    }
    
	@Override
	public void close() {
	    // close the database
	    mapDB.close();
		catalogDatabase.close();
		environment.close();
	}


	@Override
	public int add(String key, String value) {
	    Map currMap = dbViews.getMap();
	    ArrayList<String> values;
	    if(currMap.containsKey(key)){
	        values = (ArrayList<String>)currMap.get(key);
	    } else {
	        values = new ArrayList<String>();
	    }
	    values.add(value);
	    currMap.put(key, values);
		return 0;
	}


	@Override
	public List<String> get(String key) {
		return (List<String>) dbViews.getMap().get(key);
	}
	
}

