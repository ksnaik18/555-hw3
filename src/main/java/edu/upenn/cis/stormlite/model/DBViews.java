package edu.upenn.cis.stormlite.model;

import java.util.ArrayList;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.collections.StoredSortedMap;

public class DBViews {
    private StoredSortedMap map;
    

    public DBViews(Database catalogDatabase, Database mapDB){
        ClassCatalog catalog = new StoredClassCatalog(catalogDatabase);
        
        // string to user binding
        EntryBinding keyBinding = new SerialBinding(catalog, String.class);
        EntryBinding dataBinding = new SerialBinding(catalog, ArrayList.class);
        this.map = new StoredSortedMap(mapDB, keyBinding, dataBinding, true);
    
    }
    
    public StoredSortedMap getMap() {
        return this.map;
    }
    
}
